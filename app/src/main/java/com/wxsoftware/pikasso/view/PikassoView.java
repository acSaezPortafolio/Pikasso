package com.wxsoftware.pikasso.view;

import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

/**
 * Created by alberto on 27/02/18.
 */

public class PikassoView extends View {

    public static final float TOUCH_TOLERANCE = 10;

    private Bitmap bitmap;
    private Canvas bitmapCanvas;
    private Paint paintScreen;
    private Paint paintLine;

    //las figuras que se forman con los puntos que se crean al mover el dedo por la pantalla.
    private HashMap<Integer, Path> pathMap;
    //si trazamos una línea que conecten todos los puntos obtenemos un path
    private HashMap<Integer, Point> previousPointMap;




    public PikassoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        paintScreen = new Paint();
        paintLine = new Paint();
        paintLine.setAntiAlias(true);
        paintLine.setColor(Color.BLACK);
        paintLine.setStyle(Paint.Style.STROKE);
        paintLine.setStrokeWidth(7);
        paintLine.setStrokeCap(Paint.Cap.ROUND);//forma del inicio y fin del trazo


        pathMap = new HashMap<>();
        previousPointMap = new HashMap<>();
    }


    /**
     * Es llamado después del constructor pero antes de onDraw() con el objetivo de que el programa
     * calcule las dimensiones reales de la pantalla.
     * @param w
     * @param h
     * @param oldw
     * @param oldh
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        //creamos el contenedor donde vamos a poner los píxeles.
        bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        //esa configuración es la que nos permite tener el canal alfa, canal rojo, verde y azul

        //después tenemos que instanciar el canvas que es el que va a decir cómo se van a dibujar
        //las cosas, al que le pasaremos los maps para que los dibuje.
        bitmapCanvas = new Canvas(bitmap);

        bitmap.eraseColor(Color.WHITE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(bitmap, 0, 0, paintScreen);

        //dibujamos cada path almacenado en el mapa
        for (Integer key:pathMap.keySet()) {
            canvas.drawPath(pathMap.get(key), paintLine);
        }
    }


    /**
     * Listener para guardar el trazo cuando se toque la pantalla
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //se obtiene un número que simboliza el tipo de evento que se produce
        int action = event.getActionMasked();
        //el número del puntero que toca la pantalla
        int actionIndex = event.getActionIndex();


        //los motionEvent se refieren a cuando se pulsa con el primer dedo o cuando se levanta
        //el segundo dedo, de este modo cuando se pulsa la pantalla con el segundo puntero, la
        //acción anterior finaliza, y cuando se levanta comienza una nueva acción
        if (action == MotionEvent.ACTION_DOWN ||
                action == MotionEvent.ACTION_POINTER_UP) {

            //capturamos la x e y del evento que genera un puntero determinado
            touchStarted(event.getX(actionIndex), event.getY(actionIndex),
                    event.getPointerId(actionIndex));


        } else if (action == MotionEvent.ACTION_UP ||
                action == MotionEvent.ACTION_POINTER_DOWN) {

            touchEnded(event.getPointerId(actionIndex));

        }else {

            touchMoved(event);
        }

        invalidate();//fuerza a la view a redibujarse lo antes
        //posible, por lo que llama a onDraw().

        return true;
    }


    /**
     * Establece un color para la línea
     * @param color
     */
    public void setDrawingColor(int color) {
        paintLine.setColor(color);
    }

    /**
     * Devuelve el color de la línea
     * @return
     */
    public int getDrawingColor() {
        return paintLine.getColor();
    }

    /**
     * Establece la anchura de la línea
     * @param width
     */
    public void setLineWidth(int width) {
        paintLine.setStrokeWidth(width);
    }

    public int getLineWidth() {
        return (int) paintLine.getStrokeWidth();
    }


    /**
     * Limpia la pantalla dejándola en blanco
     */
    public void clear() {

        //borra todos los paths
        pathMap.clear();
        previousPointMap.clear();
        bitmap.eraseColor(Color.WHITE);
        invalidate();//refresca la pantalla
    }


    /**
     * Se va a ejecutar cuando se toca la pantalla y se mueve el dedo.
     * @param event
     */
    private void touchMoved(MotionEvent event) {

        for (int i = 0; i < event.getPointerCount(); i++) {

            int pointerId = event.getPointerId(i);
            int pointerIndex = event.findPointerIndex(pointerId);


            if (pathMap.containsKey(pointerId)) {
                //si existe, tenemos que darle al punto nuevas coordenadas
                float newX = event.getX(pointerIndex);
                float newY = event.getY(pointerIndex);

                Path path = pathMap.get(pointerId);
                Point point = previousPointMap.get(pointerId);

                //calculamos la distancia que el usuario se ha movido desde la última
                //actualización
                float deltaX = Math.abs(newX - point.x);
                float deltaY = Math.abs(newY - point.y);


                //si la distancia es lo suficiente para que sea considerado movimiento
                if (deltaX >= TOUCH_TOLERANCE || deltaY >= TOUCH_TOLERANCE) {

                    //movemos al path a la nueva localización
                    path.quadTo(point.x, point.y,
                            (newX + point.x) / 2, (newY + point.y) / 2);

                    //almacenamos las nuevas coordenadas
                    point.x = (int) newX;
                    point.y = (int) newY;
                }
            }
        }
    }


    /**
     * Se llama a este método cuando el toque de la pantalla finaliza
     * @param pointerId
     */
    private void touchEnded(int pointerId) {

        Path path = pathMap.get(pointerId);
        bitmapCanvas.drawPath(path, paintLine);
        path.reset();
    }




    /**
     * Nos dice si la acción de presión de la pantalla ha comenzado o no, lo que significa que ahora
     * estamos listos para crear el path usando los dedos en la pantalla.
     * @param x
     * @param y
     * @param pointerId
     */
    private void touchStarted(float x, float y, int pointerId) {

        //lo primero es crear el path, que va a ser el objeto a almacenar
        Path path; //almacena el path para una presión de la pantalla dada.
        Point point; //almacena el último punto en el path



        //vamos a ver si el pathMap está vacío o contiene el path con el pointer
        if (pathMap.containsKey(pointerId)) {
            path = pathMap.get(pointerId);
            point = previousPointMap.get(pointerId);
        } else {
            path = new Path();
            pathMap.put(pointerId, path);
            point = new Point();
            previousPointMap.put(pointerId, point);
        }



        path.moveTo(x, y);//establece el comienzo del siguiente contorno al punto (x,y)
        point.x = (int) x;
        point.y = (int) y;
    }


    //Da error de permisos. Se arreglaría solicitando permisos al usuario
    public void saveImage() {
        String fileName = "Pikasso" + System.currentTimeMillis();

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);
        values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");

        //obtenemos la localización del archivo
        Uri uri = getContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        try {
            OutputStream outputStream =
                    getContext().getContentResolver().openOutputStream(uri);

            //copiar el bitmap al outputstream
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            try {
                outputStream.flush();//limpia todo
                outputStream.close();//la cierra liberando memoria

                Toast.makeText(getContext(), "Imagen Guardada", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                Toast.makeText(getContext(), "Imagen No Guardada", Toast.LENGTH_SHORT).show();
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }



    public void saveToInternalStorage(){

        ContextWrapper cw = new ContextWrapper(getContext());

        String fileName = "Pikasso" + System.currentTimeMillis();

        //path: /data/data/myapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        //creamos el imageDir
        File myPath = new File(directory, fileName + ".jpg");

        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(myPath);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.flush();
                fos.close();

                Toast.makeText(cw, "Imagen Guardada", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                Toast.makeText(cw, "Imagen No Guardada", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
