package com.wxsoftware.pikasso;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.wxsoftware.pikasso.sensor.ShakeDetector;
import com.wxsoftware.pikasso.view.PikassoView;

public class MainActivity extends AppCompatActivity {

    private PikassoView pikassoView;

    private AlertDialog.Builder currentAlertDialog;

    private ImageView widthImageView;

    private AlertDialog dialogWidthLine;
    private AlertDialog colorDialog;

    private SeekBar alphaSeekBar;
    private SeekBar redSeekBar;
    private SeekBar greenSeekBar;
    private SeekBar blueSeekBar;
    private View colorView;


    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pikassoView = findViewById(R.id.view);


        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();

        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {
            @Override
            public void onShake(int count) {
                pikassoView.clear();
            }
        });

    }



    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.clearId:
                pikassoView.clear();
                break;

            case R.id.lineWidth:
                showLineWidthDialog();
                break;

            case R.id.colorId:
                showColorDialog();
                break;

            case R.id.saveId:
                pikassoView.saveImage();
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    public void showColorDialog() {

        currentAlertDialog = new AlertDialog.Builder(this);

        //contenedor del layout para poder interactuar con los elementos
        View view = getLayoutInflater().inflate(R.layout.color_dialog, null);
        alphaSeekBar = view.findViewById(R.id.alphaSeekBar);
        redSeekBar = view.findViewById(R.id.redSeekBar);
        greenSeekBar = view.findViewById(R.id.greenSeekBar);
        blueSeekBar = view.findViewById(R.id.blueSeekBar);
        colorView = view.findViewById(R.id.colorView);

        //registramos los listener para las seekbar
        alphaSeekBar.setOnSeekBarChangeListener(colorSeekBarChanged);
        redSeekBar.setOnSeekBarChangeListener(colorSeekBarChanged);
        greenSeekBar.setOnSeekBarChangeListener(colorSeekBarChanged);
        blueSeekBar.setOnSeekBarChangeListener(colorSeekBarChanged);

        //le ponemos a cada seekbar el valor del color que tenemos actualmente seleccionado
        int color = pikassoView.getDrawingColor();
        alphaSeekBar.setProgress(Color.alpha(color));
        redSeekBar.setProgress(Color.red(color));
        greenSeekBar.setProgress(Color.green(color));
        blueSeekBar.setProgress(Color.blue(color));

        Button setColorButton = view.findViewById(R.id.setColorButton);

        setColorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pikassoView.setDrawingColor(Color.argb(
                        alphaSeekBar.getProgress(),
                        redSeekBar.getProgress(),
                        greenSeekBar.getProgress(),
                        blueSeekBar.getProgress()
                ));

                colorDialog.dismiss();
            }
        });

        currentAlertDialog.setView(view);
        colorDialog = currentAlertDialog.create();
        colorDialog.show();

    }

    private SeekBar.OnSeekBarChangeListener colorSeekBarChanged = new
            SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    pikassoView.setBackgroundColor(Color.argb(
                            alphaSeekBar.getProgress(),
                            redSeekBar.getProgress(),
                            greenSeekBar.getProgress(),
                            blueSeekBar.getProgress()
                    ));

                    //mostrar el color actual
                    colorView.setBackgroundColor(Color.argb(
                            alphaSeekBar.getProgress(),
                            redSeekBar.getProgress(),
                            greenSeekBar.getProgress(),
                            blueSeekBar.getProgress()
                    ));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }

            };



    /**
     * Método que muestra el diálogo para establecer el diálogo del ancho de línea
     */
    public void showLineWidthDialog(){

        currentAlertDialog = new AlertDialog.Builder(this);

        //contenedor del layout para poder interacturar con sus elementos
        View view = getLayoutInflater().inflate(R.layout.width_dialog, null);
        final SeekBar widthSeekBar = view.findViewById(R.id.widthSeekBar);
        Button setWidthButton = view.findViewById(R.id.widthDialogButton);
        widthImageView = view.findViewById(R.id.imageViewId);

        setWidthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pikassoView.setLineWidth(widthSeekBar.getProgress());
                dialogWidthLine.dismiss();

            }
        });

        widthSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            Bitmap bitmap = Bitmap.createBitmap(400, 100, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);


            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //cambiamos el logo que sale en el dialogWidthLine por una línea con el grosor seleccionado

                Paint p = new Paint();
                p.setColor(pikassoView.getDrawingColor());
                p.setStrokeCap(Paint.Cap.SQUARE);
                p.setStrokeWidth(progress);
                bitmap.eraseColor(getResources().getColor(R.color.dialog_background));
                canvas.drawLine(30, 50, 370, 50, p);

                widthImageView.setImageBitmap(bitmap);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        widthSeekBar.setProgress(pikassoView.getLineWidth());

        currentAlertDialog.setView(view);
        dialogWidthLine = currentAlertDialog.create();
        dialogWidthLine.show();
    }
}
