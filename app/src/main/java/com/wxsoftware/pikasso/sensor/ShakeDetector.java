package com.wxsoftware.pikasso.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.util.FloatMath;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by alberto on 7/03/18.
 */

public class ShakeDetector implements SensorEventListener {


    /**
     * La fuerza para que se registre una agitación debe ser mayor a 1G (que es la gravedad
     * de la tierra)
     */



    private static final float SHAKE_THRESHOLD_GRAVITY = 2.7F;
    private static final int SHAKE_SLOP_TIME_MS = 500;
    private static final int SHAKE_COUNT_RESET_TIME_MS = 3000;

    private OnShakeListener mListener;
    private long mShakeTimestamp;
    private int mShakeCount;



    public interface OnShakeListener {
        void onShake(int count);
    }



    public void setOnShakeListener(OnShakeListener listener) {
        this.mListener = listener;
    }


    @Override
    public void onSensorChanged(SensorEvent event) {

        if (mListener != null){

            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];


            float gX = x / SensorManager.GRAVITY_EARTH;
            float gY = y / SensorManager.GRAVITY_EARTH;
            float gZ = z / SensorManager.GRAVITY_EARTH;

            //gForce será cercano a 1 cuando no haya movimiento
            float gForce = (float) Math.sqrt(gX * gX + gY * gY + gZ * gZ);

            if (gForce > SHAKE_THRESHOLD_GRAVITY){
                final long now = System.currentTimeMillis();

                //ignoramos eventos demasiado cercanos a otros(500ms)
                if (mShakeTimestamp + SHAKE_SLOP_TIME_MS > now) {
                    return;
                }

                // reseteamos el contador de agitaciones cuando pasen 3segundos desde la última
                if (mShakeTimestamp + SHAKE_COUNT_RESET_TIME_MS < now) {
                    mShakeCount = 0;
                }

                mShakeTimestamp = now;
                mShakeCount++;

                mListener.onShake(mShakeCount);
            }
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
